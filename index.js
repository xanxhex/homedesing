// Функція для перемикання теми
function toggleTheme() {
    // Отримуємо body елемент
    const body = document.body;
    
    // Перевіряємо поточну тему
    const isDarkTheme = body.classList.contains('dark-theme');
    
    // Перемикаємо клас теми
    if (isDarkTheme) {
        body.classList.remove('dark-theme');
        localStorage.setItem('theme', 'light');
    } else {
        body.classList.add('dark-theme');
        localStorage.setItem('theme', 'dark');
    }
}

// Функція для встановлення початкової теми при завантаженні сторінки
function initializeTheme() {
    const savedTheme = localStorage.getItem('theme');
    if (savedTheme === 'dark') {
        document.body.classList.add('dark-theme');
    }
}

// Додаємо кнопку для зміни теми
function addThemeToggleButton() {
    const button = document.createElement('button');
    button.textContent = 'Змінити тему';
    button.classList.add('theme-toggle');
    button.addEventListener('click', toggleTheme);
    document.body.prepend(button);
}

// Ініціалізуємо все при завантаженні сторінки
document.addEventListener('DOMContentLoaded', () => {
    initializeTheme();
    addThemeToggleButton();
});